#include <iostream>
#include <vector>
#include <fstream>
#include <string_view>
#include <random>
#include <array>
#include <cmath>
#include <chrono>
#include <functional>
#include <map>
#include "common.h"
#include "file.h"
#include "yv12_impl.h"

using file::RawReader;
using file::RawWriter;
using file::writeFrame;

/*!
 * \brief motionToChar - пребразует набор вектор движения в массив байт
 * \param motion - набор векторов движения
 * \param dst - байтовый массив
 */
void motionToChar(const std::vector<Point> &motion, std::vector<char> &dst)
{
    assert(motion.size() != dst.size() * 2);
    for (size_t i = 0; i < motion.size(); ++i) {
        dst[2 * i] = motion[i].x;
        dst[2 * i + 1] = motion[i].y;
    }
}

/*!
 * \brief motionFromChar - пребразует массив байт в набор вектор движения
 * \param src - байтовый массив
 * \param motion - набор веторов движения
 */
void motionFromChar(const std::vector<char> &src, std::vector<Point> &motion)
{
    assert(motion.size() != src.size() * 2);
    for (size_t i = 0; i < motion.size(); ++i) {
        motion[i].x = src[2 * i];
        motion[i].y = src[2 * i + 1];
    }
}

template<int N>
/*!
 * \tparam N - размер блока кадра
 * \brief writeCompensated - процедура производит поиск и компенсацию движения
 *  для всех кадров из файла rawFileName
 * \param format - заданный формат кадра
 * \param rawFileName - имя файла для обработки
 * \param compensatedFileName - имя файла для сохранения
 *  скомпенсированных кадров
 * \param motionFileName - имя файла для сохранения наборов веторов движения
 */
void writeCompensated(const YV12<N> &format,
                      std::string_view rawFileName,
                      std::string_view compensatedFileName,
                      std::string_view motionFileName)
{
    RawReader reader(rawFileName, format.frameSize());
    auto frames = reader.size() / format.frameSize();

    RawWriter writer(compensatedFileName);
    RawWriter motionWriter(motionFileName);

    auto firstFrame = reader.getFrame(0);

    writer.push(firstFrame);
    Frame secondFrame(format.frameSize());

    int avgTime = 0;
    int avgCount = 0;
    const int framesPerPercent = int(frames) / 100;
    const int remainderFrames = frames % 100;
    auto motion = format.createEmptyMotion();
    std::vector<char> charMotion(motion.size() * 2);
    Frame tempFrame(format.frameSize());

    for (size_t i = 1; i != frames; ++i) {
        if (!reader.getFrame(i, secondFrame)) {
            break;
        }

        auto begin = std::chrono::steady_clock::now();

        format.searchAndCompensate(firstFrame, secondFrame, tempFrame, motion);

        auto time = std::chrono::duration_cast<std::chrono::milliseconds>
                (std::chrono::steady_clock::now() - begin).count();

        avgTime += int(time);
        ++avgCount;

        writer.push(tempFrame);
        std::swap(firstFrame, secondFrame);

        motionToChar(motion, charMotion);
        motionWriter.push(charMotion);

        if ( framesPerPercent && (i - remainderFrames + 1) % framesPerPercent == 0) {
            std::cout << i * 100 / frames + 1
                      << "% current average time per frame: "
                      << avgTime / avgCount << " ms" << std::endl;
            avgTime = 0;
            avgCount = 0;
        }
    }
}

template<int N>
/*!
 * \tparam N - размер блока кадра
 * \brief writeRestored - процедура производит восстановление
 *  видеопоследовательности и сравнение с исходной видеопоследовательностью
 * \param format - format
 * \param compesatedFileName - имя файла для обработки
 * \param motionFileName - имя файла наборов скомпенсированных кадров
 * \param restoredFileName - имя файла для сохранения восстановленной
 *  видеополедовательности
 * \param originalFileName - оригинальная видеопоследовательность
 */
void writeRestored(const YV12<N> &format,
//                   const std::vector<std::vector<Point>> &motion,
                   std::string_view compesatedFileName,
                   std::string_view motionFileName,
                   std::string_view restoredFileName,
                   std::string_view originalFileName)
{
    RawReader reader(compesatedFileName, format.frameSize());
    auto motion = format.createEmptyMotion();
    std::vector<char> charMotion(motion.size() * 2);
    RawReader motionReader(motionFileName, charMotion.size());
    RawWriter writer(restoredFileName);
    RawReader origReader(originalFileName, format.frameSize());
    auto frames = reader.size() / format.frameSize();
    auto firstFrame = reader.getFrame(0);
    writer.push(firstFrame);
    Frame diffFrame(format.frameSize());
    Frame tempFrame(format.frameSize());
    Frame origFrame(format.frameSize());

    for (size_t i = 1; i != frames; ++i) {
        reader.getFrame(i, diffFrame);
        motionReader.getFrame(i - 1, charMotion);
        motionFromChar(charMotion, motion);
        format.motionDecompensation(motion, diffFrame,
                                    firstFrame, tempFrame);
        writer.push(tempFrame);
        origReader.getFrame(i, origFrame);

        std::cout << "frame " << i + 1 << " of " << frames
                  << " psnr: " << format.psnr(tempFrame, origFrame)
                  << std::endl;

        std::swap(firstFrame, tempFrame);
    }
}

// пример использования MotionCompensation -h 576 -w 720 yuv420.raw

int main ([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    int width = 0;
    int height = 0;
    const std::map<std::string_view, std::function<void(char*)>>
    params{
        {"-w", [&width](char *s) { width = strtol(s, nullptr, 10);} },
        {"-h", [&height](char *s) { height = strtol(s, nullptr, 10);} }
    };
    std::string_view fileName;
    for (int i = 1; i < argc; ++i) {
        auto it = params.find(argv[i]);
        if (it != params.end() && (i + 1 < argc )) {
            it->second(argv[++i]);
        }
        else {
            fileName = argv[i];
        }
    }
    if (width < 2 || height < 2 || fileName.empty()) {
        std::cout << "invalid options" << std::endl;
        return 0;
    }
    std::cout << "height: " << height << std::endl;
    std::cout << "width: " << width << std::endl;
    YV12<8> format(height, width);
//    YV12<16> format(height, width);
    std::cout << std::endl;

    auto frameSize = format.frameSize();
    RawReader reader(fileName, frameSize);

    std::cout << std::endl << "Writing the compensated file..." << std::endl;

    writeCompensated(format, fileName,
                     "compensated_file.raw", "motion_file.vec");

    std::cout << std::endl << "Writing the restored file..." << std::endl;

    writeRestored(format, "compensated_file.raw", "motion_file.vec",
                  "restored_file.raw", fileName);

    std::cout << "complete" << std::endl;
    return 0;
}

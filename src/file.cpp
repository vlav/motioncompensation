#include <iostream>
#include "file.h"
namespace file {
RawReader::RawReader(std::string_view fileName, size_t frameSize)
    : m_fileStream(std::string(fileName), std::ios::binary|std::ios::ate)
    , m_frameSize(frameSize)
    , m_fileSize(0)
{
    if (!m_fileStream) {
        return;
    }
    auto end = m_fileStream.tellg();
    m_fileStream.seekg(0, std::ios::beg);
    m_fileSize = std::size_t(end - m_fileStream.tellg());
}

Frame RawReader::getFrame(size_t n)
{
    Frame frame(m_frameSize, 0);
    if (getFrame(n, frame)) {
        return frame;
    }
    return {};
}

bool RawReader::getFrame(size_t n, Frame &frame)
{

    auto position = m_frameSize * n;

    if (m_fileSize < position + m_frameSize
            || !m_frameSize || frame.size() != m_frameSize) {

        return false;
    }

    m_fileStream.seekg(position, std::ios::beg);

    if (!m_fileStream.read(frame.data(), m_frameSize)) {
        return false;
    }

    return true;
}

size_t RawReader::size()
{
    return m_fileSize;
}

RawWriter::RawWriter(std::string_view fileName)
    : m_fileStream(std::string(fileName), std::ios::binary)
{
}

void RawWriter::push(const Frame &frame)
{
    m_fileStream.write(frame.data(), frame.size());
}

void writeFrame(const Frame &frame, std::string_view fileName)
{
    std::ofstream output(std::string(fileName), std::ios::binary );
    output.write(reinterpret_cast<const char*>(frame.data()), frame.size());
}

} //file

QT -= gui core

unix: QMAKE_CXXFLAGS_GNUCXX1Z = -std=c++17
win32: QMAKE_CXXFLAGS_GNUCXX1Z = /std:c++17

unix: QMAKE_CXXFLAGS += -Wno-unknown-pragmas
unix: QMAKE_CXXFLAGS += -Wno-unused-parameter
unix: QMAKE_CXXFLAGS += -Wno-sign-compare
unix: QMAKE_CXXFLAGS += -std=c++17
win32: QMAKE_CXXFLAGS += /std:c++17

unix: QMAKE_CXX = g++-9

CONFIG += c++1z console
CONFIG -= app_bundle
DEFINES += QT_DEPRECATED_WARNINGS

DESTDIR = $$PWD/../bin

INCLUDEPATH += $$PWD/../include

HEADERS += \
    ../include/common.h \
    ../include/file.h \
    ../include/yv12.h \
    ../include/yv12_impl.h

SOURCES += \
    ../src/file.cpp \
    ../src/main.cpp


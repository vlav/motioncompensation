#ifndef COMMON_H
#define COMMON_H
#include <vector>

// Данные кадра
using Frame = std::vector<char>;

/**
 * @brief Координаты точки или вектора
 */
struct Point
{
    int x;
    int y;
    constexpr Point(): x(0), y(0) {}
    constexpr Point(int x_, int y_): x(x_), y(y_) {}
    constexpr Point(const Point &) = default;
    constexpr Point &operator=(const Point &) = default;
};

#endif //COMMON_H

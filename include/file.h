#ifndef FILE_H
#define FILE_H
#include <string_view>
#include <string>
#include <fstream>

#include "common.h"

namespace file {

/**
 * @brief Класс чтения поледовательности кадров
 */
class RawReader
{
public:

    /*!
     * \brief RawReader
     * \param fileName - имя файла
     * \param frameSize - размер кадра
     */
    RawReader(std::string_view fileName, size_t frameSize);

    /*!
     * \brief getFrame
     * \param n - номер кадра в файле
     * \return данные кадра, в случае отсутствия кадра возвращает пустой
     */
    Frame getFrame(size_t n);

    /*!
     * \brief getFrame
     * \param n - номер кадра в файле
     * \param frame - ссылка на данные кадра
     * \return возвращет был ли считан кадр
     */
    bool getFrame(size_t n, Frame &frame);

    /*!
     * \brief size
     * \return возвращает размер файла
     */
    size_t size();

private:
    std::ifstream m_fileStream;
    size_t m_frameSize;
    size_t m_fileSize;
};

/**
 * @brief Класс записи поледовательности кадров
 */
class RawWriter
{
public:

    /*!
     * \brief RawWriter
     * \param fileName - имя файла
     */
    RawWriter(std::string_view fileName);

    /*!
     * \brief push - помещает кадр в файл
     * \param frame - кадр для записи
     */
    void push(const Frame &frame);

private:
    std::ofstream m_fileStream;
};

/*!
 * \brief writeFrame - запись отдельного кадра в файл
 * \param frame - данные кадра
 * \param fileName - имя файла
 */
void writeFrame(const Frame &frame, std::string_view fileName);

} //file
#endif //FILE_H

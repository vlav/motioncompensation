#ifndef YV12_IMPL_H
#define YV12_IMPL_H

#include <vector>
#include <cmath>
#include <thread>
#include <array>
#include <limits>

#include <assert.h>

#include "yv12.h"

template<int BlockSize>
int YV12<BlockSize>::height() const
{
    return m_height;
}

template<int BlockSize>
int YV12<BlockSize>::width() const
{
    return m_width;
}

template<int BlockSize>
int YV12<BlockSize>::frameSize() const
{
    return m_frameSize;
}

template<int BlockSize>
char YV12<BlockSize>::getV(const Frame &frame, int x, int y) const
{
    return frame[m_lumaSize + (y>>1) * m_chromaWidth + (x>>1)];
}

template<int BlockSize>
char YV12<BlockSize>::getU(const Frame &frame, int x, int y) const
{
    return frame[m_lumaSize + m_chromaSize + (y>>1) * m_chromaWidth + (x>>1)];
}

template<int BlockSize>
void YV12<BlockSize>::setV(Frame &frame, int x, int y, char v) const
{
    frame[m_lumaSize + (y>>1) * m_chromaWidth + (x>>1)] = v;
}

template<int BlockSize>
void YV12<BlockSize>::setU(Frame &frame, int x, int y, char u) const
{
    frame[m_lumaSize + m_chromaSize + (y>>1) * m_chromaWidth + (x>>1)] = u;
}

template<int BlockSize>
void YV12<BlockSize>::
motionDecompensation(const std::vector<Point> &motionVectors,
                     const Frame &frame2, const Frame &frame1,
                     Frame &frame3) const
{
    int blockNumber = 0;
    Block block1, block2;
    for (int i = 0; i < m_hInBlocks; ++i) {
        int y2 = i * BlockSize;
        for (int j = 0; j < m_wInBlocks; ++j) {
            int x2 = j * BlockSize;
            readBlock_s(x2, y2, frame2, block2);
            const Point &mv = motionVectors[blockNumber];
            bool isOk = readByHalfPixel(mv.x + (x2 << 1), mv.y + (y2 << 1),
                                        frame1, block1);
            assert(isOk); (void)isOk;
            decompensate(block2, block1);
            writeBlock(x2, y2, block2, frame3);
            ++blockNumber;
        }
    }
    copyDerelict(frame2, frame3);
}

template<int BlockSize>
std::vector<Point> YV12<BlockSize>::createEmptyMotion() const
{
    return std::vector<Point> (m_hInBlocks * m_wInBlocks, {0,0});
}

template<int BlockSize>
void YV12<BlockSize>::
searchAndCompensate(const Frame &frame1, const Frame &frame2,
                          Frame &frame3, std::vector<Point> &motion) const
{


    int threadCount = std::thread::hardware_concurrency();
    int blockLinesPerThread = m_hInBlocks / threadCount;
    int yBegin = 0;
    int yEnd = 0;
    std::vector<std::thread> threads;
    for(int i = 0; i < threadCount - 1; ++i) {
        yBegin = blockLinesPerThread * i;
        yEnd = yBegin + blockLinesPerThread;
        threads.emplace_back(&YV12::scm, this, yBegin, yEnd,
                             std::ref(frame1), std::ref(frame2),
                             std::ref(frame3), std::ref(motion));
    }
    yBegin = yEnd;
    yEnd = m_hInBlocks;
    scm(yBegin, yEnd, frame1, frame2, frame3, motion);

    for(auto &thread: threads) {
        thread.join();
    }

    copyDerelict(frame2, frame3);
}

template<int BlockSize>
double YV12<BlockSize>::psnr(const Frame &frame1, const Frame &frame2) const
{
    long long se = 0;
    for(int i = 0; i < m_frameSize; ++i) {
        int d = int (frame2[i] - frame1[i]);
        se += d * d;
    }
    return 10 * std::log10( 255 * 255 / (double(se) / m_frameSize));
}

template<int BlockSize>
void YV12<BlockSize>::
compensate(YV12::Block &block2, const YV12::Block &block1) const
{
    for(int i = 0; i < blockBytes; ++i) {
        block2[i] -= block1[i];
    }
}

template<int BlockSize>
void YV12<BlockSize>::
decompensate(YV12::Block &block2, const YV12::Block &block1) const
{
    for(int i = 0; i < blockBytes; ++i) {
        block2[i] += block1[i];
    }
}

template<int BlockSize>
void YV12<BlockSize>::copyDerelict(const Frame &src, Frame &dst) const
{
    //Передаём необработанные остатки как есть

    //right
    //luma
    for (int y = 0; y < m_height; ++y){
        for (int x = m_wInBlocks * BlockSize; x < m_width; ++x) {
            int index = y*m_width + x;
            dst[index] = src[index];
        }
    }
    //chroma
    for (int y = 0; y < m_height; y += 2) {
        for (int x = m_wInBlocks * BlockSize; x < m_width; x += 2) {
            setV(dst, x, y, getV(src, x, y));
            setU(dst, x, y, getU(src, x, y));
        }
    }

    //bottom
    //luma
    for (int y = m_hInBlocks * BlockSize; y < m_height; ++y){
        for (int x =0; x < m_wInBlocks * BlockSize; ++x) {
            int index = y*m_width + x;
            dst[index] = src[index];
        }
    }
    //chroma
    for (int y = m_hInBlocks * BlockSize; y < m_height; y += 2) {
        for (int x = 0; x < m_wInBlocks * BlockSize; x += 2) {
            setV(dst, x, y, getV(src, x, y));
            setU(dst, x, y, getU(src, x, y));
        }
    }

}

template<int BlockSize>
bool YV12<BlockSize>::
readBlock_s(int x, int y, const Frame &frame, YV12::Block &block) const
{
    if ( y < 0 || x < 0 ||
         x + BlockSize > m_width || y + BlockSize > m_height ) {

        return false;
    }
    readBlock(x, y, frame, block);
    return true;
}

template<int BlockSize>
void YV12<BlockSize>::
readBlock(int x, int y, const Frame &frame, YV12::Block &block) const
{
    int offset = y * m_width + x;
    int blockOffset = 0;
    int frameLineOffset = 0;
    for(int i = 0; i < BlockSize; ++i) {
        for(int j = 0; j < BlockSize; ++j) {
            block[blockOffset] = frame[offset + frameLineOffset +j];
            ++blockOffset;
        }
        frameLineOffset +=  m_width;
    }

    constexpr int lumaBlockSize = BlockSize * BlockSize;
    constexpr int chromaSideSize = (BlockSize + 1) >> 1;
    constexpr int chromaBlockSize = chromaSideSize * chromaSideSize;
    constexpr int vBegin = lumaBlockSize;
    constexpr int uBegin = vBegin + chromaBlockSize;

    offset = (y >> 1) * m_chromaWidth + (x >> 1);

    blockOffset = 0;
    frameLineOffset = 0;
    for(int i = 0; i < chromaSideSize; ++i) {
        for(int j = 0; j < chromaSideSize; ++j) {
            block[vBegin + blockOffset] =
                frame[offset + m_lumaSize + frameLineOffset +j];

            block[uBegin + blockOffset] =
                frame[offset + m_lumaSize + m_chromaSize + frameLineOffset +j];

            ++blockOffset;
        }
        frameLineOffset += m_chromaWidth;
    }
}

template<int BlockSize>
bool YV12<BlockSize>::
writeBlock(int x, int y, const YV12::Block &block, Frame &frame) const
{
    if ( y < 0 || x < 0 ||
         x + BlockSize > m_width || y + BlockSize > m_height ) {

        return false;
    }

    int offset = y * m_width + x;

    for(int i = 0; i < BlockSize; ++i) {
        for(int j = 0; j < BlockSize; ++j) {
            frame[offset + i * m_width +j] = block[i * BlockSize + j];
        }
    }

    constexpr int lumaBlockSize = BlockSize * BlockSize;
    constexpr int vuSideSize = (BlockSize + 1) >> 1;
    constexpr int chromaBlockSize = vuSideSize * vuSideSize;
    constexpr int vBegin = lumaBlockSize;
    constexpr int uBegin = vBegin + chromaBlockSize;

    offset = (y >> 1) * m_chromaWidth + (x >> 1);
    for(int i = 0; i < vuSideSize; ++i) {
        for(int j = 0; j < vuSideSize; ++j) {
            frame[offset + m_lumaSize + i * m_chromaWidth +j] =
                    block[vBegin + i * vuSideSize + j];
            frame[offset + m_lumaSize + m_chromaSize + i * m_chromaWidth +j] =
                    block[uBegin + i * vuSideSize + j];
        }
    }
    return true;
}

template<int BlockSize>
int YV12<BlockSize>::sad(const YV12::Block &b1, const YV12::Block &b2) const
{
    int s = 0;
    for(int i = 0; i < blockBytes; ++i) {
        s += std::abs( int((unsigned char) (b1[i])) - int((unsigned char)(b2[i])));
    }
    return s;
}

template<int BlockSize>
void YV12<BlockSize>::
correctMargins(int &left, int &top, int &right, int &bottom) const
{
    if (left < 0) {
        left = 0;
    }
    if (right + BlockSize > m_width) {
        right = m_width - BlockSize;
    }
    if (top < 0) {
        top = 0;
    }
    if (bottom + BlockSize > m_height) {
        bottom = m_height -  BlockSize;
    }
}

template<int BlockSize>
int YV12<BlockSize>::fullSearch(Point &p0, const Block &b2,
                                 const Frame &frame1) const
{
    int left = p0.x - m_searchSize;
    int top = p0.y - m_searchSize;
    int right = p0.x + m_searchSize;
    int bottom = p0.y + m_searchSize;

    correctMargins(left, top, right, bottom);

    int error = std::numeric_limits<int>::max();
    Block b1;
    int nx = p0.x;
    int ny = p0.y;
    for (int y1 = top; y1 < bottom; y1++) {
        for (int x1 = left; x1 < right; x1++) {
            if (readBlock_s(x1, y1, frame1, b1)) {
                int e = sad(b1, b2);
                if (e < error) {
                    error = e;
                    nx = x1;
                    ny = y1;
                }
            }
        }
    }
    p0.x = nx;
    p0.y = ny;
    return error;
}

template<int BlockSize>
Point YV12<BlockSize>::smallHexagonSearch(int x1, int y1, int &error,
                                          const Frame &frame1, YV12::Block &b1,
                                          const YV12::Block &b2) const
{
    constexpr std::array<Point, 4> small{
        Point{1,0}, Point{0,1},
        Point{-1,0}, Point{0,-1}
    };
    int x = x1;
    int y = y1;
    for(const Point &p: small) {
        if (readBlock_s(p.x + x1, p.y + y1, frame1, b1)) {
            int e = sad(b2, b1);
            if (e < error) {
                error = e;
                x = x1 + p.x;
                y = y1 + p.y;
            }
        }
    }
    return Point{x, y};
}

template<int BlockSize>
void YV12<BlockSize>::
halfPixelSearch(Point &p0, int error, const YV12::Block &b2,
                const Frame &frame1) const
{
    Block b1;
    int nx = p0.x;
    int ny = p0.y;
    constexpr std::array<Point, 8> points = []()
    {
        std::array<Point, 8> p;
        size_t ndx = 0;
        for (int y = -1; y < 2; ++y) {
            for (int x = -1; x < 2; ++x) {
                if ( x != 0 && y != 0) {
                    p[ndx++] = Point{x,y};
                }
            }
        }
        return p;
    }();

    for (const Point &p: points) {
        int x1 = p0.x + p.x;
        int y1 = p0.y + p.y;
        if (readByHalfPixel(x1, y1, frame1, b1)) {
            int e = sad(b1, b2);
            if (e < error) {
                nx = x1;
                ny = y1;
                error = e;
            }
        }
    }
    p0.x = nx;
    p0.y = ny;
}

template<int BlockSize>
void YV12<BlockSize>::scm(int yBegin, int yEnd, const Frame &frame1,
                          const Frame &frame2, Frame &frame3,
                          std::vector<Point> &motion) const
{
    Block b1, b2;
    for (int i = yBegin; i < yEnd; ++i) {
        for (int j = 0; j < m_wInBlocks; ++j ) {
            int x2 = j * BlockSize;
            int y2 = i * BlockSize;
            readBlock_s(x2, y2, frame2, b2);
            Point &mv = motion[i * m_wInBlocks + j];
            Point p{x2,y2};
            int lastSAD =
//                fullSearch(p, b2, frame1);
                hexagonSearch(p, b2, frame1);

            p.x = p.x << 1;
            p.y = p.y << 1;
            halfPixelSearch(p, lastSAD, b2, frame1);
            mv.x = p.x - (x2 << 1);
            mv.y = p.y - (y2 << 1);
            bool isOk = readByHalfPixel(p.x, p.y, frame1, b1);
            assert(isOk); (void)isOk;

            compensate(b2, b1);
            writeBlock(x2, y2, b2, frame3);
        }
    }
}

template<int BlockSize>
bool YV12<BlockSize>::
readByHalfPixel(int hx, int hy, const Frame &frame, YV12::Block &block) const
{
    constexpr int lumaBlockSize = BlockSize * BlockSize;
    constexpr int chromaSideSize = (BlockSize + 1) >> 1;
    constexpr int chromaBlockSize = chromaSideSize * chromaSideSize;
    constexpr int vBegin = lumaBlockSize;
    constexpr int uBegin = vBegin + chromaBlockSize;

    if ((hy < 0) || (hx < 0)) {
        return false;
    }

    int y = hy >> 1;
    int x = hx >> 1;
    int dx = hx & 1;
    int dy = hy & 1;

    if (!dx && !dy) {
        if ( x + BlockSize > m_width || y + BlockSize > m_height ) {
            return false;
        }
        readBlock(x,y, frame, block);
        return true;
    }

//    if ( (((x >> 1) + 1) + chromaSideSize - 1) >= m_chromaWidth
//         || (((y >> 1)  + 1)  + chromaSideSize - 1) >= m_cromaHeight    ) {
    if ( (x >> 1) + chromaSideSize >= m_chromaWidth
         || (y >> 1) + chromaSideSize >= m_chromaHeight ) {

        return false;
    }

    int offset = y * m_width + x;
    int blockOffset = 0;
    int frmLine = 0;
    for (int i = 0; i < BlockSize; ++i) {
        for (int j = 0; j < BlockSize; ++j) {
            int Y00 = frame[offset + frmLine + j];
            int Y01 = frame[offset + frmLine + j + 1];
            int Y10 = frame[offset + m_width + frmLine + j];
            int Y11 = frame[offset + m_width + frmLine + j];
            int b1 = Y00;
            int b2 = Y01 - Y00;
            int b3 = Y10 - Y00;
            int b4 = Y00 - Y01 - Y10 + Y11;
            block[blockOffset] =
                b1 + ((((b2 * dx)<< 1) + ((b3 * dy)<<1) + b4 * dx * dy) >>2);

            ++blockOffset;
        }
        frmLine +=  m_width;
    }


    offset = (y >> 1) * m_chromaWidth + (x >> 1);
    dx = hx - ((x >> 1) << 2);
    dy = hy - ((y >> 1) << 2);
    blockOffset = 0;
    frmLine = 0;
    for (int i = 0; i < chromaSideSize; ++i) {
        for (int j = 0; j < chromaSideSize; ++j) {
            int C00 = frame[offset + m_lumaSize + frmLine + j];
            int C01 = frame[offset + m_lumaSize + frmLine + j + 1];
            int C10 = frame[offset + m_lumaSize + m_chromaWidth + frmLine + j];
            int C11 = frame[offset + m_lumaSize + m_chromaWidth + frmLine + j + 1];
            int b1 = C00;
            int b2 = C01 - C00;
            int b3 = C10 - C00;
            int b4 = C00 - C01 - C10 + C11;

            block[vBegin + blockOffset] = b1 + ((((b2 * dx)<<2) + ((b3 * dy)<<2) + b4 * dx * dy) >> 4);
            C00 = frame[offset + m_lumaSize + m_chromaSize + frmLine +j];
            C01 = frame[offset + m_lumaSize + m_chromaSize + frmLine +j + 1];
            C10 = frame[offset + m_lumaSize + m_chromaSize + m_chromaWidth + frmLine +j];
            C11 = frame[offset + m_lumaSize + m_chromaSize + m_chromaWidth + frmLine +j + 1];
            b1 = C00;
            b2 = C01 - C00;
            b3 = C10 - C00;
            b4 = C00 - C01 - C10 + C11;

            block[uBegin + blockOffset] = b1 + ((((b2 * dx)<<2) + ((b3 * dy)<<2) + b4 * dx * dy) >> 4);
            ++blockOffset;
        }
        frmLine += m_chromaWidth;
    }
    return true;
}

template<int BlockSize>
int YV12<BlockSize>::findNewHexagonPoints(int direction,
                int &error, int x1, int y1,
                const Hexagon &large,
                const Frame &frame1,
                YV12::Block &b1, const YV12::Block &b2) const
{
    int result = 0;
    for (int i = 0; i < 3; ++i) {
        int newDirection = (direction + i + 4) % 6 + 1;
        const auto &p = large[newDirection - 1];
        int nx = x1 + p.x;
        int ny = y1 + p.y;

        if (readBlock_s(nx, ny, frame1, b1)) {
            int e = sad(b2, b1);
            if (e < error) {
                error = e;
                result = newDirection;
            }
        }
    }
    return result;
}

template<int BlockSize>
int YV12<BlockSize>::hexagonSearch(Point &p0, const YV12::Block &b2,
                                    const Frame &frame1) const
{
    int x2 = p0.x;
    int y2 = p0.y;
    constexpr Hexagon large{
        Point{2,0}, Point{1,2}, Point{-1,2},
        Point{-2,0}, Point{-1,2}, Point{1,-2}
    };

    Block b1;
    int error = std::numeric_limits<int>::max();
    int direction = 0;
    int x1 = x2;
    int y1 = y2;

    //get error of center point
    if (readBlock_s(x1, y1, frame1, b1)) {
        error = sad(b2, b1);
    }
    else {
        assert(false);
    }

    //find 3 right points
    direction = findNewHexagonPoints(1, error, x1, y1, large, frame1, b1, b2);
    {   //find 3 left points
        int n = findNewHexagonPoints(4, error, x1, y1, large, frame1, b1, b2);
        if (n != 0) {
            direction = n;
        }
    }

    const int squareRadius = m_searchSize * m_searchSize;
    while(direction) {
        //set new center
        const auto &p = large[direction - 1];
        x1 += p.x;
        y1 += p.y;
        int mvx = x2 - x1;
        int mvy = y2 - y1;
        int smv = mvx * mvx + mvy * mvy;
        if (smv > squareRadius) {
            direction = 0;
        }
        else {
            direction = findNewHexagonPoints(direction, error, x1, y1, large,
                                             frame1, b1, b2);
        }
    }
    Point p = smallHexagonSearch(x1, y1, error, frame1, b1, b2);
    p0.x = p.x;
    p0.y = p.y;
    return error;
}


#endif //YV12_IMPL_H

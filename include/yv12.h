#ifndef YV12_H
#define YV12_H

#include <tmmintrin.h>
#include <iostream>
#include "common.h"

// Координаты точек шестиугольника
using Hexagon = std::array<Point, 6>;

/*!
 * \brief YV12 Класс для работы с изображениями(кадрами) формата YV12
 *
 */
template <int BlockSize>
class YV12
{
public:

    /*!
     * \brief Размер блока изображения в байтах
     */
    static constexpr int blockBytes =
            BlockSize * BlockSize +
            ((((BlockSize + 1) >> 1) * ((BlockSize + 1) >> 1)) << 1);

    using Block = char[blockBytes];
    /**< Блок изображения */

    /*!
     * \brief YV12 Конструктор класса для работы с изоборажениями формата YV12
     * \param height - высота изображения
     * \param width - ширина изображения
     * \param searchSize - размер области поиска движения (радиус поиска)
     */
    YV12(int height, int width, int searchSize = 16)
        : m_height(height), m_width(width)
        , m_lumaSize(height*width)
        , m_chromaHeight((m_height + 1) >> 1), m_chromaWidth((m_width + 1) >> 1)
        , m_chromaSize(m_chromaHeight * m_chromaWidth)
        , m_frameSize(m_lumaSize + (m_chromaSize<<1))
        , m_searchSize(searchSize)
        , m_hInBlocks(m_height / BlockSize)
        , m_wInBlocks(m_width / BlockSize)
    {
    }
    ~YV12(){}

    /*!
     * \brief height
     * \return возвращает высоту кадра
     */
    int height() const;

    /*!
     * \brief width
     * \return возвращает ширину кадра
     */
    int width() const;

    /*!
     * \brief frameSize
     * \return возвращает размер кадра в байтах
     */
    int frameSize() const;

    /*!
     * \brief getV - получить V компоненту точки
     * \param frame - данные кадра
     * \param x - x координата точки
     * \param y - y координата точки
     * \return V компонента
     */
    char getV(const Frame &frame, int x, int y) const;

    /*!
     * \brief getV - получить U компоненту точки
     * \param frame - данные кадра
     * \param x - x координата точки
     * \param y - y координата точки
     * \return V компонента
     */
    char getU(const Frame &frame, int x, int y) const;

    /*!
     * \brief setV - установить V компоненту точки
     * \param frame - данные кадра
     * \param x - x координата точки
     * \param y - y координата точки
     * \param v - V компонента
     */
    void setV(Frame &frame, int x, int y, char v) const;

    /*!
     * \brief setU - установить U компоненту точки
     * \param frame - данные кадра
     * \param x - x координата точки
     * \param y - y координата точки
     * \param v - U компонента
     */
    void setU(Frame &frame, int x, int y, char u) const;

    /*!
     * \brief motionDecompensation - процедура декомпенсации движения
     * \param motionVectors - набор векторов движения для кадра frame2
     * \param frame2 - данные скоменсированного кадра
     * \param frame1 - данные опорного кадра
     * \param frame3 - восстановленное изображение
     */
    void motionDecompensation(const std::vector<Point> &motionVectors,
                                   const Frame &frame2,
                                   const Frame &frame1,
                                   Frame &frame3) const;


    /*!
     * \brief createEmptyMotion - создать набор нулевых векторов движения
     * \return набор нулевых векторов движения
     */
    std::vector<Point> createEmptyMotion() const;

    /*!
     * \brief searchAndCompensate - процедура поиска и компенсации движения
     * \param frame1 - опорный кадр
     * \param frame2 - компеснируемый кадр
     * \param frame3 - скомпенсированный кадр
     * \param motion - вектор движения
     */
    void searchAndCompensate(const Frame &frame1, const Frame &frame2,
                                   Frame &frame3,
                                   std::vector<Point> &motion) const;
    /*!
     * \brief psnr - функция рассчета пикового отношения сигнала к шуму
     * \param frame1 - первое изображение
     * \param frame2 - второе изображение
     * \return PSNR
     */
    double psnr(const Frame &frame1, const Frame &frame2) const;

    /*!
     * \brief fullSearch - процедура полного поиска движения
     * \param p0 - на входе точка начала поиска на выходе найденные координаты
     * начала движения
     * \param b2 - блок изображения для которого ищем движение
     * \param frame1 - опорный кадр
     * \return возвращает значение функция оценки схожести блоков в найденной
     *  точке
     */
    int fullSearch(Point &p0, const Block &b2,
                    const Frame &frame1) const;

    /*!
     * \brief hexagonSearch - процедура поиска движения
     *  Алгоритм гексагонального поиска
     * \param p0 - на входе точка начала поиска на выходе найденные координаты
     * начала движения
     * \param b2 - блок изображения для которого ищем движение
     * \param frame1 - опорный кадр
     * \return возвращает значение функция оценки схожести блоков в найденной
     *  точке
     */
    int hexagonSearch(Point &p0, const Block &b2,
                       const Frame &frame1) const;


    /*!
     * \brief halfPixelSearch - процедура полупиксельного поиска движения
     * \param p0 - на входе точка начала поиска на выходе найденные координаты
     * начала движения
     * координты точки являются удвоенными значениями
     * \param error - значение функция оценки в точке начало поиска
     * \param b2 - блок изображения для которого ищем движение
     * \param frame1 - опорный кадр
     */
    void halfPixelSearch(Point &p0, int error, const Block &b2,
                         const Frame &frame1) const;

private:

    /*!
     * \brief m_height - высота кадра в пикселях
     */
    const int m_height;

    /*!
     * \brief m_width - ширина кадра в пикселях
     */
    const int m_width;

    /*!
     * \brief m_lumaSize - размер данных отведенных под яркость
     */
    const int m_lumaSize;

    /*!
     * \brief m_chromaHeight - высота кадра для компонент цветности
     */
    const int m_chromaHeight;

    /*!
     * \brief m_chromaWidth - ширина кадра для компонент цветности
     */
    const int m_chromaWidth;

    /*!
     * \brief m_chromaSize - размер данных отведенных под комоненту цветности
     */
    const int m_chromaSize;

    /*!
     * \brief m_frameSize - размер кадра в байтах
     */
    const int m_frameSize;

    /*!
     * \brief m_searchSize - размер области поиска движения (радиус поиска)
     */
    const int m_searchSize;

    /*!
     * \brief m_hInBlocks - высота кадра в блоках
     */
    const int m_hInBlocks;

    /*!
     * \brief m_wInBlocks - ширина кадра в блоках
     */
    const int m_wInBlocks;

    /*!
     * \brief compensate - процедура компенсации, производит
     *  попиксельное вычитание блоков
     * \param block2 - на входе блок из которого усществляется вычетание,
     * на выходе результат
     * \param block1 - вычетаемый блок
     */
    void compensate(Block &block2, const Block &block1) const;

    /*!
     * \brief compensate - процедура декомпенсации
     * \param block2 - на входе компенсированный блок,
     * на выходе восстановленный блок
     * \param block1 - блок опорного кадра
     */
    void decompensate(Block &block2, const Block &block1) const;

    /*!
     * \brief copyDerelict - копировние не обработанных  остатков изображения
     * \param src - источник
     * \param dst - место назначения
     */
    void copyDerelict(const Frame &src, Frame &dst) const;

    /*!
     * \brief readBlock - процедура чтения блока изображения
     * \param x - x координата левой верхней точки блока
     * \param y - y координата левой верхней точки блока
     * \param frame - кадр, из которого прозводится чтение
     * \param block - блок в который помещаются считанный данные
     */
    void readBlock(int x, int y, const Frame &frame, Block &block) const;

    /*!
     * \brief readBlock_s - чтение блока с провекой выхода за пределы кадра
     * \param x - x координата левой верхней точки блока
     * \param y - y координата левой верхней точки блока
     * \param frame - кадр, из которого прозводится чтение
     * \param block - блок в который помещаются считанный данные
     * \return true - чтение произошло, false - вышли за пределы кадра
     */
    bool readBlock_s(int x, int y, const Frame &frame, Block &block) const;

    /*!
     * \brief writeBlock - запись блока по указанным координатам
     * \param x - x координата левой верхней точки блока
     * \param y - y координата левой верхней точки блока
     * \param block - записываемый блок
     * \param frame - кадр, в который прозводится запись
     * \return true - запись произошла, false - вышли за пределы кадра
     */
    bool writeBlock(int x, int y, const  Block &block, Frame &frame) const;

    /*!
     * \brief correctMargins - корректировка границ поиска
     *  для метода полного поиска
     * \param left
     * \param top
     * \param right
     * \param bottom
     */
    void correctMargins(int &left, int &top, int &right, int &bottom) const;

    /*!
     * \brief sad - функция подсчета суммы абсолютных разностей для блоков
     * \param b1 - первый блок
     * \param b2 - второй блок
     * \return сумма абсолютных разностей для блоков
     */
    int sad(const Block &b1, const Block &b2) const;

    /*!
     * \brief findNewHexagonPoints - процедура нахождения новых точек
     * для алгоритма гесогонального поиска
     * \param direction - направление движения шестиугольника
     * \param error - текущая оценки схожести блоков
     * \param x1 - x координата центра шестиугольника
     * \param y1 - x координата центра шестиугольника
     * \param large - массив точек шестиугольник в относительных координатах
     * \param frame1 - изображение в котором осуществляется поиск
     * \param b1 - найденный блок
     * \param b2 - блок по котром осуществляем поиск
     * \return новое направление поиска
     */
    int findNewHexagonPoints (int direction, int &error,
        int x1, int y1,
        const std::array<Point, 6> &large,
        const Frame &frame1, Block &b1, const Block &b2) const;

    /*!
     * \brief smallHexagonSearch - процедура завершающая поиск
     * по шестиугольнику
     * \param x1 - x координата центра шестиугольника
     * \param y1 - x координата центра шестиугольника
     * \param error - текущая оценки схожести блоков
     * \param frame1 - изображение в котором осуществляется поиск
     * \param b1 - найденный блок
     * \param b2 - блок по котром осуществляем поиск
     * \return найденная точка
     */
    Point smallHexagonSearch(int x1, int y1, int &error,
                             const Frame &frame1,
                             Block &b1, const Block &b2) const;

    /*!
     * \brief scm - процедура поиска и компенсации движения в части кадра
     * \param yBegin - начальная y координата части кадра
     * \param yEnd - конечная y координата части кадра (yBegin <= y < yEnd)
     * \param frame1 - опорный кадр
     * \param frame2 - компеснируемый кадр
     * \param frame3 - скомпенсированный кадр
     * \param motion - найденный вектор движения
     */
    void scm(int yBegin, int yEnd,
             const Frame &frame1, const Frame &frame2,
             Frame &frame3, std::vector<Point> &motion) const;

    /*!
     * \brief readByHalfPixel - чтение блока по полупиксельным координатам
     * \param hx - x координата левой верхней точки блока в полупикселях
     * \param hy - н координата левой верхней точки блока в полупикселях
     * \param frame
     * \param block
     * \return
     */
    bool readByHalfPixel(int hx, int hy, const Frame &frame, Block &block) const;
};


/*!
 * \brief YV12<8>::readBlock - шаблон функции readBlock с полной специализацией
 *  шаблона класса YV12 для работы с типом блоком 8x8 пикселей
 *  с использованием SIMD инстркуций
 * \param x - x координата левой верхней точки блока
 * \param y - y координата левой верхней точки блока
 * \param frame - кадр, из которого прозводится чтение
 * \param block - блок в который помещаются считанный данные
 */
template<>
void YV12<8>::readBlock(int x, int y, const Frame &frame, Block &block) const
{
    int offset = y * m_width + x;
    int blockOffset = 0;
    int frameLineOffset = 0;
    constexpr int BlockSize = 8;
    for(int i = 0; i < BlockSize; ++i) {
        const __m128i line = _mm_loadl_epi64(reinterpret_cast<const __m128i *> (&frame[offset + frameLineOffset]));
        _mm_storel_epi64(reinterpret_cast<__m128i *>(&block[blockOffset]), line);
        blockOffset += BlockSize;
        frameLineOffset +=  m_width;
    }

    constexpr int lumaBlockSize = BlockSize * BlockSize;
    constexpr int chromaSideSize = 4;
    constexpr int chromaBlockSize = chromaSideSize * chromaSideSize;
    constexpr int vBegin = lumaBlockSize;
    constexpr int uBegin = vBegin + chromaBlockSize;

    offset = (y >> 1) * m_chromaWidth + (x >> 1);

    blockOffset = 0;
    frameLineOffset = 0;
    for(int i = 0; i < chromaSideSize; ++i) {
        *reinterpret_cast<uint32_t *>(&block[vBegin + blockOffset]) =
            *reinterpret_cast<const uint32_t *>(&frame[offset + m_lumaSize + frameLineOffset]);

         *reinterpret_cast<uint32_t *>(&block[uBegin + blockOffset]) =
             *reinterpret_cast<const uint32_t *>(&frame[offset + m_lumaSize + m_chromaSize + frameLineOffset]);

        blockOffset += chromaSideSize;
        frameLineOffset += m_chromaWidth;
    }

}

/*!
 * \brief YV12<8>::compensate шаблон функции compensate с полной специализацией
 *  шаблона класса YV12 для работы с типом блоком 8x8 пикселей
 *  с использованием SIMD инстркуций
 * \param block2 - на входе блок из которого усществляется вычетание,
 * на выходе результат
 * \param block1 - вычетаемый блок
 */
template<>
void YV12<8>::compensate(Block &block2, const Block &block1) const
{
    for (int i = 0; i < 96; i += 16) {
        __m128i b1 = _mm_loadu_si128(reinterpret_cast<const __m128i *> (&block1[i]));
        __m128i b2 = _mm_loadu_si128(reinterpret_cast<__m128i *> (&block2[i]));
        b2 = _mm_sub_epi8(b2, b1);
        _mm_store_si128((__m128i *) &block2[i], b2);
    }
}


template<>
/*!
 * \brief YV12<8>::sad шаблон функции sad с полной специализацией
 *  шаблона класса YV12 для работы с типом блоком 8x8 пикселей
 *  с использованием SIMD инстркуций
 * \param buf1 - первый блок
 * \param buf2 - второй блок
 * \return сумма абсолютных разностей для блоков
 */
int YV12<8>::sad(const Block &buf1, const Block &buf2) const
{
    //https://habr.com/ru/company/intel/blog/242781/
    __m128i sum = _mm_setzero_si128();
    for (int i = 0; i < 96; i += 16) {
        __m128i b1 = _mm_loadu_si128(reinterpret_cast<const __m128i *> (&buf1[i]));
        __m128i b2 = _mm_loadu_si128(reinterpret_cast<const __m128i *> (&buf2[i]));
        __m128i s1 = _mm_sad_epu8(b1, b2);
        sum = _mm_add_epi32(sum, s1);
    }
        sum = _mm_hadd_epi32(sum, sum);
        sum = _mm_hadd_epi32(sum, sum);
    return _mm_cvtsi128_si32(sum);
}

#endif //YV12_H
